libtest-bdd-cucumber-perl (0.86-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.86.
  * d/control: depend on libcucumber-tagexpressions-perl.
  * typo.patch: new: fix typo caught by lintian.

 -- Étienne Mollier <emollier@debian.org>  Tue, 29 Aug 2023 21:41:31 +0200

libtest-bdd-cucumber-perl (0.84-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.84.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 29 Jul 2023 17:34:54 +0200

libtest-bdd-cucumber-perl (0.83-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.83.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Update lintian override (syntax change).

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Nov 2022 18:46:53 +0100

libtest-bdd-cucumber-perl (0.82-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 05:56:01 +0100

libtest-bdd-cucumber-perl (0.82-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.82.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 28 Aug 2021 17:50:53 +0200

libtest-bdd-cucumber-perl (0.81-1) unstable; urgency=medium

  * Team upload.
  * Import upstream versions 0.77, 0.79, 0.81
  * Update years of upstream copyright.
  * Update test and runtime dependencies.
  * Add patch to use installed pherkin binary during autopkgtests.

 -- gregor herrmann <gregoa@debian.org>  Mon, 16 Aug 2021 18:04:44 +0200

libtest-bdd-cucumber-perl (0.75-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.75.
  * Remove workaround for backup files which are removed from the
    upstream tarball.

 -- gregor herrmann <gregoa@debian.org>  Wed, 30 Dec 2020 17:24:54 +0100

libtest-bdd-cucumber-perl (0.74-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.74.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Mon, 14 Dec 2020 18:50:28 +0100

libtest-bdd-cucumber-perl (0.73-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.73.
  * Add another file to debian/copyright.
  * Add workarounds for leftover backup files in the tarball.
    Don't remove one of them during clean, don't install the other one.

 -- gregor herrmann <gregoa@debian.org>  Tue, 01 Sep 2020 19:11:32 +0200

libtest-bdd-cucumber-perl (0.72-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.72.
  * Update Upstream-Contact and upstream copyright notices.
  * Drop pherkin-set-interpreter-to-usr-bin-perl.patch:
    not needed anymore.
  * Update lintian override (renamed tag).
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 24 Aug 2020 02:59:14 +0200

libtest-bdd-cucumber-perl (0.71-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.71.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 23:42:52 +0200

libtest-bdd-cucumber-perl (0.70-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.70.

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Apr 2020 18:43:18 +0200

libtest-bdd-cucumber-perl (0.69-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.69.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Set upstream metadata fields: Bug-Submit.

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Apr 2020 01:26:50 +0200

libtest-bdd-cucumber-perl (0.67-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.67.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Sep 2019 02:53:05 +0200

libtest-bdd-cucumber-perl (0.64-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Import upstream version 0.62.
  * Update buildtime and runtime dependencies.

  [ intrigeri ]
  * Import upstream version 0.64.
  * Explicitly list dependencies provided by perl.
  * Override a Lintian false positive.

 -- intrigeri <intrigeri@debian.org>  Tue, 17 Sep 2019 08:08:43 +0000

libtest-bdd-cucumber-perl (0.60-1) unstable; urgency=medium

  * Team upload.
  * Import upstream versions 0.58, 0.59, 0.60.
  * Update GitHub URLs in debian/upstream/metadata.
  * Remove debian/libtest-bdd-cucumber-perl.docs,
    the upstream TODO is gone.

 -- gregor herrmann <gregoa@debian.org>  Sun, 01 Sep 2019 23:26:17 +0200

libtest-bdd-cucumber-perl (0.57-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.57.
  * Update debian/upstream/metadata.
  * Update years of upstream copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Jul 2019 16:05:11 -0300

libtest-bdd-cucumber-perl (0.56-3) unstable; urgency=medium

  * Team upload.
  * Drop override_dh_auto_install from debian/rules.
    Not necessary, as README.pod isn't installed anymore with Perl 5.28, and
    leads to a build failure now.
    Thanks to Niko Tyni for detecting and reporting. (Closes: #915297)

 -- gregor herrmann <gregoa@debian.org>  Sun, 02 Dec 2018 17:46:57 +0100

libtest-bdd-cucumber-perl (0.56-2) unstable; urgency=medium

  * New patch: pherkin-set-interpreter-to-usr-bin-perl.patch.
  * Declare compliance with Debian Policy 4.2.1.
  * Don't install duplicate /usr/share/perl5/Test/BDD/README.pod.
  * Bump debhelper compatibility level to 11.

 -- intrigeri <intrigeri@debian.org>  Mon, 29 Oct 2018 10:00:12 +0000

libtest-bdd-cucumber-perl (0.56-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.56.
  * Update years of upstream copyright.
  * Make (build) dependency on libmoo-perl versioned.
  * Declare compliance with Debian Policy 4.1.4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 May 2018 15:42:35 +0200

libtest-bdd-cucumber-perl (0.53-1) unstable; urgency=medium

  * Import new upstream release: Moose → Moo.
  * Update {build,runtime} dependencies: libmoose-perl → libmoo-perl
    + libmoox-handlesvia-perl + libtype-tiny-perl.
  * debian/control: update comment about libdevel-findref-perl.
  * Declare compliance with Standards-Version 4.0.0 (no change required).
  * Bump debhelper compat level to 10, adjust build-dependencies accordingly.
  * Revert "Don't include ParserOld.pm in the binary package": the upstream
    tarball was fixed.

 -- intrigeri <intrigeri@debian.org>  Mon, 24 Jul 2017 05:54:29 +0000

libtest-bdd-cucumber-perl (0.52-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ intrigeri ]
  * Import new upstream release.
  * Drop {build,run}-time dependency on libfile-slurp-perl.
  * debian/copyright: bump years for upstream copyright.
  * debian/copyright: bump years for debian/*.
  * debian/rules: delete trailing whitespace.
  * Don't include ParserOld.pm in the binary package.

 -- intrigeri <intrigeri@debian.org>  Sat, 24 Jun 2017 10:42:06 +0000

libtest-bdd-cucumber-perl (0.50-1) unstable; urgency=medium

  * Team upload.

  [ Lucas Kanashiro ]
  * Import upstream version 0.50
  * Declare compliance with Debian policy 3.9.8

  [ gregor herrmann ]
  * Drop {alternative,versioned} (build) dependencies already satisfied in
    oldstable.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 02 May 2016 23:53:14 -0300

libtest-bdd-cucumber-perl (0.49-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.49

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Wed, 09 Mar 2016 23:46:32 -0300

libtest-bdd-cucumber-perl (0.48-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.48
  * d/u/metadata: fix Contact field
  * Drop spelling.patch, not needed anymore

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sun, 28 Feb 2016 18:59:21 -0300

libtest-bdd-cucumber-perl (0.45-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Import upstream version 0.45.
  * Update years of upstream copyright.
  * Add third-party copyright information to debian/copyright.
  * Declare compliance with Debian Policy 3.9.7.
  * Add build dependency on libtest-exception-perl.
  * Add a patch to fix spelling mistakes in the documentation.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Feb 2016 14:47:59 +0100

libtest-bdd-cucumber-perl (0.40-1) unstable; urgency=medium

  * Import new upstream release.

 -- intrigeri <intrigeri@debian.org>  Sun, 03 Jan 2016 10:51:48 +0000

libtest-bdd-cucumber-perl (0.39-2) unstable; urgency=medium

  * Team upload.

  [ intrigeri ]
  * Drop build-dependency on libdevel-findref-perl. It's optional since
    upstream release 0.36, and broken on Perl 5.22
    (https://rt.cpan.org/Ticket/Display.html?id=101077). Let's avoid
    Test::BDD::Cucumber being removed from the archive because of that.
  * Downgrade runtime dependency on libdevel-findref-perl to Suggests.

  [ gregor herrmann ]
  * Comment out Suggest: libdevel-findref-perl.
    Suggests stops autopkgtests's syntax.t.

 -- gregor herrmann <gregoa@debian.org>  Fri, 30 Oct 2015 22:12:21 +0100

libtest-bdd-cucumber-perl (0.39-1) unstable; urgency=medium

  * Team upload.
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.
  * Import upstream version 0.39

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Oct 2015 17:20:12 +0100

libtest-bdd-cucumber-perl (0.37-1) unstable; urgency=medium

  * New upstream release.

 -- intrigeri <intrigeri@debian.org>  Fri, 28 Aug 2015 00:20:32 +0200

libtest-bdd-cucumber-perl (0.35-1) unstable; urgency=medium

  * New upstream release.
    - Fixed Test::Builder wrapping issue
    - Shutdown harness gracefully
    - Minor documentation fixes
    - Updated repository location
  * debian/upstream/metadata: update upstream bug database and repository.
  * Add {runtime,build} dependency on libdevel-findref-perl
    and libdevel-refcount-perl.
  * debian/copyright: bump copyright years for the packaging.

 -- intrigeri <intrigeri@debian.org>  Tue, 28 Jul 2015 16:27:13 +0200

libtest-bdd-cucumber-perl (0.34-1) unstable; urgency=medium

  * New upstream release.
  * Update upstream copyright years.

 -- intrigeri <intrigeri@debian.org>  Mon, 27 Apr 2015 11:38:25 +0200

libtest-bdd-cucumber-perl (0.31-1) unstable; urgency=medium

  * Team upload.

  [ intrigeri ]
  * New upstream release: 0.30.

  [ gregor herrmann ]
  * Imported upstream version 0.31
  * Make (build) dependency on libfile-slurp-perl versioned.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Oct 2014 23:28:45 +0200

libtest-bdd-cucumber-perl (0.29-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ intrigeri ]
  * New upstream release, including:
    - Add JSON output support
    - pherkin now has a --help command line switch
  * Remove {build,runtime}-dependency on libfindbin-libs-perl.

 -- intrigeri <intrigeri@debian.org>  Wed, 27 Aug 2014 12:09:37 -0700

libtest-bdd-cucumber-perl (0.26-1) unstable; urgency=medium

  * New upstream release.
  * Add new {runtime,build}-dependency on libio-stringy-perl,
    that provides IO::Scalar.
  * Add debian/upstream/metadata.

 -- intrigeri <intrigeri@debian.org>  Sat, 21 Jun 2014 12:08:53 +0200

libtest-bdd-cucumber-perl (0.25-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add (build) dependency on libnumber-range-perl.

 -- gregor herrmann <gregoa@debian.org>  Thu, 12 Jun 2014 18:09:49 +0200

libtest-bdd-cucumber-perl (0.24-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop pod.patch, fixed upstream.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Jun 2014 14:58:57 +0200

libtest-bdd-cucumber-perl (0.23-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update years of upstream copyright.
  * Update (build) dependencies.
  * Add a patch to fix POD error.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Jun 2014 14:03:17 +0200

libtest-bdd-cucumber-perl (0.18-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ intrigeri ]
  * Imported Upstream version 0.18
  * Declare compliance with standards-version 3.9.5.

 -- intrigeri <intrigeri@debian.org>  Thu, 10 Apr 2014 08:14:11 +0200

libtest-bdd-cucumber-perl (0.17-1) unstable; urgency=medium

  * New upstream release, including these noteworthy changes:
    - Supress colours by default when outputting to not a tty
    - Support Before and After Hooks
    - Support tagged scenarios

 -- intrigeri <intrigeri@debian.org>  Fri, 06 Dec 2013 15:42:10 +0100

libtest-bdd-cucumber-perl (0.15-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Don't install almost empty boilerplate README anymore.
  * Update years of copyright.
  * Rewrite dependency on Term::ANSIColor 3.00.
  * Declare compliance with Debian Policy 3.9.4.
  * Fix hashbang in example scripts.

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Jul 2013 20:31:49 +0200

libtest-bdd-cucumber-perl (0.14-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ intrigeri ]
  * Imported Upstream version 0.14

 -- intrigeri <intrigeri@debian.org>  Sun, 12 May 2013 23:31:44 +0200

libtest-bdd-cucumber-perl (0.11-1) unstable; urgency=low

  * Imported Upstream version 0.11
  * Add versioned dependency on perl-modules to get Term::ANSIColor 3.00.

 -- intrigeri <intrigeri@debian.org>  Sun, 03 Jun 2012 14:28:55 +0200

libtest-bdd-cucumber-perl (0.10-1) unstable; urgency=low

  * Imported Upstream version 0.10
  * Update dependencies: libclone-fast-perl -> libclone-perl.

 -- intrigeri <intrigeri@debian.org>  Sat, 19 May 2012 12:03:03 +0200

libtest-bdd-cucumber-perl (0.09-1) unstable; urgency=low

  * Imported Upstream version 0.08.
  * Upload to Debian (Closes: #670148)

 -- intrigeri <intrigeri@debian.org>  Sun, 29 Apr 2012 15:51:59 +0200

libtest-bdd-cucumber-perl (0.08-1) unstable; urgency=low

  * Imported Upstream version 0.08.

 -- intrigeri <intrigeri@debian.org>  Mon, 23 Apr 2012 15:40:59 +0200

libtest-bdd-cucumber-perl (0.07-1) UNRELEASED; urgency=low

  * Imported Upstream version 0.07.
  * Add new dependencies on libclone-fast-perl and liblist-moreutils-perl.
  * Add sane package description.

 -- intrigeri <intrigeri@debian.org>  Mon, 23 Apr 2012 10:47:34 +0200

libtest-bdd-cucumber-perl (0.05-1) UNRELEASED; urgency=low

  * Imported Upstream version 0.05

 -- intrigeri <intrigeri@debian.org>  Tue, 20 Mar 2012 02:23:35 +0100

libtest-bdd-cucumber-perl (0.04-1) UNRELEASED; urgency=low

  * Initial Release.

 -- intrigeri <intrigeri@debian.org>  Sun, 4 Mar 2012 10:57:51 +0100
